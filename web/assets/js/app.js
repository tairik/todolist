/**
 * Created by Tairik on 2016-06-28.
 */

$("#sortable").sortable();
$("#sortable").disableSelection();

countTodos();

// all done btn
$("#checkAll").click(function(){
    AllDone();
});

//create todo
$('.add-todo').on('keypress',function (e) {
    e.preventDefault
    if (e.which == 13) {
        if($(this).val() != ''){
            var todo = $(this).val();
            createToDo(todo);
            countTodos();
        }else{
            // some validation
        }
    }
});
// mark task as done
$('.todolist').on('click','#sortable li input[type="checkbox"]',function(){
    if($(this).prop('checked')){

        var id = $(this).prop('id');
        markAsCompleted(id);
        var doneItem = $(this).parent().parent().find('label').text();
        var id = $(this).prop('id');
        $(this).parent().parent().parent().parent().addClass('remove');
        done(doneItem, id);
        countTodos();


    }
});

//edit task
$('.todolist').on('click','.edit-item',function(){
    var id = $(this).data("id");
    if($('#todo_label-label-'+id).is(':visible')) {
        $('#todo_label-input-'+id).show();
        $('#todo_label-label-'+id).hide();
    } else {
        $('#todo_label-input-'+id).hide();
        $('#todo_label-label-'+id).show();
    }
});

//edit task - send
$('body').on('keypress', '.todo_label', function (e) {
//$('.todo_label').bind('keypress',function (e) {
    e.preventDefault
    if (e.which == 13) {
        if($(this).val() != ''){
            var todo = $(this).val();
            var id   = $(this).data("id");
            updateToDo(todo, id);
        }else{
            // some validation
        }
    }
});


//delete done task from "already done"
$('.todolist').on('click','.remove-item',function(){
    //console.log($(this));
    removeItem(this);
});

// count tasks
function countTodos(){
    var count = $("#sortable li").length;
    $('.count-todos').html(count);
}

function markAsCompleted(id){
    $.ajax({
        type: "POST",
        url: '/api/todo/completed/'+id,
        success: function(data, textStatus, jqXHR)
        {

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

//update task
function updateToDo(text, id){
    $.ajax({
        type: "POST",
        url: '/api/todo/'+id,
        data: 'label='+text,
        success: function(data, textStatus, jqXHR)
        {
            $('#todo_label-input-'+id).val(text);
            $('#todo_label-input-'+id).hide();
            $('#todo_label-label-'+id).show();
            var markup = '<input id="'+id+'" type="checkbox" value="'+id+'" />'+text;
            $('#todo_label-label-'+id).html(markup);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });

}

//create task
function createToDo(text){
    $.ajax({
        type: "POST",
        url: '/api/todo/',
        data: 'label='+text,
        success: function(data, textStatus, jqXHR)
        {
            var markup = '<li class="ui-state-default"><div class="checkbox row"><div class="col-md-10"><label id="todo_label-label-'+data.id+'"><input type="checkbox" value="'+text+'" id="'+data.id+'" />'+ text +'</label><input type="text" class="form-control todo_label" name="todo_label" data-id="'+data.id+'" id="todo_label-input-'+data.id+'" value="'+text+'" style="display: none"></div><div class="col-md-2"><button class="edit-item btn btn-default btn-xs pull-right" data-id="'+data.id+'"><span class="glyphicon glyphicon-edit"></span></button></div></div></li>';
            $('#sortable').append(markup);
            $('.add-todo').val('');
            countTodos();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

//mark task as done
function done(doneItem, id){
    var done = doneItem;
    var id   = id;
    var markup = '<li>'+ done +'<button class="btn btn-default btn-xs pull-right  remove-item" id="'+id+'"><span class="glyphicon glyphicon-remove"></span></button></li>';
    $('#done-items').append(markup);
    $('.remove').remove();
}

//mark all tasks as done
function AllDone(){
    var myArray    = [];
    var myArrayIds = [];

    $('#sortable li').each( function() {
        myArray.push($(this).text());
    });
    $('#sortable li').each( function() {
        myArrayIds.push($(this).find('label').next().data('id'));
    });

    // add to done
    for (i = 0; i < myArray.length; i++) {
        markAsCompleted(myArrayIds[i]);
        $('#done-items').append('<li>' + myArray[i] + '<button class="btn btn-default btn-xs pull-right  remove-item" id="'+myArrayIds[i]+'"><span class="glyphicon glyphicon-remove"></span></button></li>');
    }

    // myArray
    $('#sortable li').remove();
    countTodos();
}

//remove done task from list
function removeItem(element){

    $.ajax({
        type: "DELETE",
        url: '/api/todo/delete/'+$(element).prop('id'),
        success: function(data, textStatus, jqXHR)
        {
            $(element).parent().remove();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}