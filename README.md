ToDoList - Web Application
========

A Symfony project created on June 28, 2016, 3:17 pm.

1) Notes:
---------------------------------

    This project pretends to delivery a "ToDoList" Web Application based on Symfony 3.0, MySQL, Bootstrap, jQuery hosted by Heroku.

    a) I decided to create a bundle called "ToDoList" since this can be a part of an application that will be using other bundles.
    b) The controller API exposes the data rendering the output in JSON format
    c) I created an API Listener to be able to catch every request made on API Controller. A basic security implementation was made but we can use: OAuth 2.0 in the future
    d) I created a JsonExceptionListener to be able to catch and handle the exceptions that may throw in the API Controller
    e) Unit and functional Tests must be implemented
    f) You will find the references that I used in the list of links below


2) Heroku and Symfony2 With MySQL
----------------------------------

    https://coderwall.com/p/qpitzq/deploing-symfony-project-using-mysql-to-heroku

3) Best Practice To Get Request Object Symfony2
----------------------------------

    http://stackoverflow.com/questions/20984816/what-is-the-best-way-to-get-the-request-object-in-the-controller

4) Pre Dispatch with Symfony2
----------------------------------

    http://stackoverflow.com/questions/7293075/how-to-create-a-something-like-zend-predispatch-method-in-symfony2
    http://symfony.com/doc/current/cookbook/event_dispatcher/event_listener.html

5) Custom Excpetion Symfony2
----------------------------------

    http://stackoverflow.com/questions/21932229/how-can-i-throw-a-403-exception-in-symfony2
    http://stackoverflow.com/questions/21787482/custom-exception-behavior-in-symfony2

