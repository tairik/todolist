<?php
/**
 * Created by PhpStorm.
 * User: Tairik
 * Date: 2016-07-01
 * Time: 11:42 AM
 */

namespace ToDoListBundle\EventListener;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class JsonExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $data = array(
            'error' => array(
                'code' => 403,
                'message' => $exception->getMessage()
            )
        );
        $response = new JsonResponse($data);
        $event->setResponse($response);
    }
}