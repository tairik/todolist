<?php

namespace ToDoListBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\RequestStack;


class ApiListener
{
    protected $objRequest;

    public function setRequest(RequestStack $request_stack)
    {
        $this->objRequest = $request_stack->getCurrentRequest();
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        if(HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) {
            $controllers = $event->getController();
            if(is_array($controllers)) {
                $controller = $controllers[0];

                if(is_object($controller) && method_exists($controller, 'preExecute')) {
                    $controller->preExecute($this->objRequest);
                }
            }
        }
    }
}