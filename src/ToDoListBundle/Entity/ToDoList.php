<?php

namespace ToDoListBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ToDoList
 *
 * @ORM\Table(name="list")
 * @ORM\Entity(repositoryClass="ToDoListBundle\Repository\ToDoListRepository")
 */
class ToDoList
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="todo_label", type="string", length=255)
     */
    private $todoLabel;

    /**
     * @var bool
     *
     * @ORM\Column(name="completed", type="boolean")
     */
    private $completed;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set todoLabel
     *
     * @param string $todoLabel
     *
     * @return ToDoList
     */
    public function setTodoLabel($todoLabel)
    {
        $this->todoLabel = $todoLabel;

        return $this;
    }

    /**
     * Get todoLabel
     *
     * @return string
     */
    public function getTodoLabel()
    {
        return $this->todoLabel;
    }

    /**
     * Set completed
     *
     * @param boolean $completed
     *
     * @return ToDoList
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * Get completed
     *
     * @return bool
     */
    public function getCompleted()
    {
        return $this->completed;
    }
}

