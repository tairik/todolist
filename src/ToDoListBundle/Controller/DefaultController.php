<?php

namespace ToDoListBundle\Controller;

use MongoDB\Driver\Exception\ExecutionTimeoutException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Config\Definition\Exception\Exception;
use ToDoListBundle\Entity\ToDoList;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="todolistHomepage")
     */
    public function indexAction()
    {
        $list              = $this->getDoctrine()->getRepository('ToDoListBundle:ToDoList')->findAllArray();
        $aryToView         = array();
        $aryToView['list'] = $list;

        return $this->render('ToDoListBundle:Default:index.html.twig', $aryToView);
    }

    
}
