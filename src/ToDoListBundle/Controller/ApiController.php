<?php
/**
 * Created by PhpStorm.
 * User: Tairik
 * Date: 2016-06-30
 * Time: 12:39 AM
 */

namespace ToDoListBundle\Controller;

use MongoDB\Driver\Exception\ExecutionTimeoutException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Config\Definition\Exception\Exception;
use ToDoListBundle\Entity\ToDoList;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


class ApiController extends Controller
{

    /**
     * @param Request $objRequest
     *
     * @return boolean
     */
    private function authenticate(Request $objRequest)
    {
        $host = $this->container->getParameter('host');
        $aryHeaders = $objRequest->headers->all();
        $return     = false;
        if ($aryHeaders['host'][0] === $host) {
            $return = true;
        }

        return $return;
    }

    /**
     * @param Request $objRequest
     * @throws AccessDeniedHttpException
     */
    public function preExecute(Request $objRequest)
    {
        $testUser = $this->authenticate($objRequest);
        if (false === $testUser) {
            throw new AccessDeniedHttpException('No token given, token is wrong or host invalid.');
        }
    }

    /**
     * @Route("/api/todo/")
     * @Method({"GET","HEAD"})
     */
    public function listAction(Request $objRequest)
    {
        $list                = $this->getDoctrine()->getRepository('ToDoListBundle:ToDoList')->findAllArray();
        $aryResponse         = array();
        $aryResponse['list'] = $list;
        return new JsonResponse($aryResponse);
    }

    /**
     * @Route("/api/todo/")
     * @Method({"POST","HEAD"})
     */
    public function addToDoAction(Request $objRequest)
    {
        $aryResponse = array();
        try {
            $objToDoList = new ToDoList();
            $objToDoList->setTodoLabel($objRequest->get('label'));
            $objToDoList->setCompleted(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($objToDoList);
            $em->flush();
            $aryResponse['status'] = 'Ok';
            $aryResponse['id']     = $objToDoList->getId();
        } catch (Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return new JsonResponse($aryResponse);
    }

    /**
     * @Route("/api/todo/{id}")
     * @Method({"POST","HEAD"})
     */
    public function updateToDoAction(Request $objRequest)
    {
        $aryResponse = array();
        try {
            $objToDoList = $this->getDoctrine()->getRepository('ToDoListBundle:ToDoList')->find($objRequest->get("id"));
            $objToDoList->setTodoLabel($objRequest->get("label"));
            $em = $this->getDoctrine()->getManager();
            $em->persist($objToDoList);
            $em->flush();
            $aryResponse['status'] = 'Ok';
        } catch (Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return new JsonResponse($aryResponse);
    }

    /**
     * @Route("/api/todo/completed/{id}")
     * @Method({"POST","HEAD"})
     */
    public function completedToDoAction(Request $objRequest)
    {
        $aryResponse = array();
        try {
            $objToDoList = $this->getDoctrine()->getRepository('ToDoListBundle:ToDoList')->find($objRequest->get("id"));
            $objToDoList->setCompleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->persist($objToDoList);
            $em->flush();
            $aryResponse['status'] = 'Ok';
        } catch (Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return new JsonResponse($aryResponse);
    }

    /**
     * @Route("/api/todo/delete/{id}")
     * @Method({"DELETE","HEAD"})
     */
    public function deleteAction(Request $objRequest)
    {
        $aryResponse = array();
        try {
            $objToDoList = $this->getDoctrine()->getRepository('ToDoListBundle:ToDoList')->find($objRequest->get("id"));
            $em = $this->getDoctrine()->getManager();
            $em->remove($objToDoList);
            $em->flush();
            $aryResponse['status'] = 'Ok';
        } catch (Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return new JsonResponse($aryResponse);
    }
}