<?php

namespace ToDoListBundle\Repository;

/**
 * ToDoListRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ToDoListRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllArray()
    {

        $strSQL = 'SELECT TD.id, TD.todoLabel, TD.completed
                            FROM ToDoListBundle\Entity\ToDoList TD
                            ';
        $query = $this->_em->createQuery($strSQL);
        $result = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $result;

    }
}
